﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Microsoft.Extensions.Configuration;


namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class DataContext
        : DbContext
    {
        public DbSet<PromoCode> PromoCodes { get; set; }

        public DbSet<Customer> Customers { get; set; }
        
        public DbSet<Preference> Preferences { get; set; }
        
        public DbSet<Role> Roles { get; set; }
        
        public DbSet<Employee> Employees { get; set; }

        public DataContext()
        {
            
        }
        
        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CustomerPreference>()
                .HasKey(bc => new { bc.CustomerId, bc.PreferenceId });  
            modelBuilder.Entity<CustomerPreference>()
                .HasOne(bc => bc.Customer)
                .WithMany(b => b.Preferences)
                .HasForeignKey(bc => bc.CustomerId);  
            modelBuilder.Entity<CustomerPreference>()
                .HasOne(bc => bc.Preference)
                .WithMany()
                .HasForeignKey(bc => bc.PreferenceId); 
        }
    }

    //=====================================================================================================
    public class AppDbContextFactory : IDesignTimeDbContextFactory<DataContext>
    {
        private readonly IConfiguration _configuration;

        public AppDbContextFactory()
        {
        }
        public AppDbContextFactory(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public DataContext CreateDbContext(string[] args)
        {
            var OptionsBuilder = new DbContextOptionsBuilder<DataContext>();
            //OptionsBuilder.UseSqlite(@"F:\PROJECTS\OtusPromo\DockerGitLab\src\Otus.Teaching.PromoCodeFactory.WebHost");
            OptionsBuilder.UseNpgsql(@"Server=pg;Port=5432;Database=PromoFactoryDB;UserId=docker;Password=postgres");
            return new DataContext(OptionsBuilder.Options);
        }
    }
   
}